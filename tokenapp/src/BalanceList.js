import React, { Component } from 'react';

class BalanceList extends Component {


  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.balanceList.map((member,k)=>{
          return <div key={k} style={{margin:"0.25em",borderRadius:"1em",border:"1px solid blue"}}>
              Address : {member.address}  Balance : {member.balance}
          </div>
        })}
      </div>
    );
  }
}

export default BalanceList;
