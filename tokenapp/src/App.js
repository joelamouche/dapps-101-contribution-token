import React, { Component } from 'react';
import logo from './thumbsup.png';
import './App.css';

import Web3 from 'web3'

import ContributionToken from './ContributionToken.json'

import SendTokens from './SendTokens'
import BalanceList from './BalanceList'

class App extends Component {

  constructor(props) {
    super(props);
    this.state={
      status:"Loading...",
      userAddress:"",
      tokenAddress:"",
      web3:{},
      ContractInstance:{},
      balance:0,
      balanceList:[]
    }
  }

  async componentDidMount () {
    // load networkID and user address to use as rootKey for the data persistor

    // Modern dapp browsers...
    let web3
    if ((window)['ethereum']) {
      web3 = new Web3((window)['ethereum'])
      try {
        // Request account access if needed
        await (window)['ethereum'].enable()
      } catch (error) {
        // User denied account access...
        // TODO setup 'user rejection modal'
        console.log('user rejection')
        this.setState({ status: 'UserRejection' })
      }
    } else if ((window)['web3']) { // Legacy dapp browsers...
      web3 = new Web3((window )['web3'].currentProvider)
      // Acccounts always exposed
    } else { // Non-dapp browsers...
      console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
      this.setState({ status: 'NoMetaMask' })// TODO this modal shouldnt be able to be closed
    }

    let accounts = await web3.eth.getAccounts()
    let networkID = await web3.eth.net.getId()


    if (accounts.length === 0) {
      this.setState({ status: 'UnlockMetaMask' })
    } else {
      let tokenAddress=ContributionToken.networks[networkID].address

      let ContractInstance=await new web3.eth.Contract(ContributionToken.abi, tokenAddress)

      let balance=await ContractInstance.methods.balanceOf(accounts[0]).call()

      let pastevents = await ContractInstance.getPastEvents('Transfer', {
        fromBlock: 0,
        toBlock: "latest"
      })

      let balanceList=[]

      await Promise.all(pastevents.map(async (transfer)=>{
        let recipient=transfer.returnValues[1]
        if (!balanceList.includes(recipient)){
          let recBalance=await ContractInstance.methods.balanceOf(recipient).call()
          balanceList.push({address:recipient,balance:recBalance})
        }
      }))

      this.setState({ 
        status: 'Account Loaded', 
        userAddress: accounts[0], 
        web3:web3,
        ContractInstance : ContractInstance,
        tokenAddress: tokenAddress,
        balance,
        balanceList
      })
    }
  }

  sendTransaction(amount,recipient){
    console.log("send",amount,recipient)
    this.state.ContractInstance.methods.transfer(recipient,amount).send({from:this.state.userAddress},()=>{
      console.log("transaction callback")
    })
  }


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div style={{marginBottom:"3em",fontSize:"2em"}}>{"Antoine's Contributors"}</div>
          <img style={{height:"7em"}} src={logo} alt="logo" />
          <div style={{marginTop:"3em"}}>{this.state.status}</div>
          <div style={{marginTop:"1em"}}>{"USER ADDRESS : "+this.state.userAddress}</div>
          <div style={{marginTop:"1em"}}>{"USER BALANCE : "+this.state.balance}</div>
          <div style={{marginTop:"1em"}}>{"TOKEN ADDRESS : "+this.state.tokenAddress}</div>
        </header>
        <SendTokens sendTransaction={this.sendTransaction.bind(this)} />
        <BalanceList balanceList={this.state.balanceList} />
      </div>
    );
  }
}

export default App;
