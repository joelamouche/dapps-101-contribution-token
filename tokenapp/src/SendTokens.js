import React, { Component } from 'react';

class SendTokens extends Component {


  constructor(props) {
    super(props);
    this.state={
      amount:0,
      address:""
    }
  }

  componentDidMount() {

  }

  setAmount(e) {
    this.setState({amount:Number(e.target.value)});
  }

  setRecipientAddress(e) {
    this.setState({address:e.target.value});
  }

  sendTokens(){
    if (this.state.amount>0 && this.state.address!==""){
      this.props.sendTransaction(this.state.amount,this.state.address)
    } else {
      window.alert("Please enter amount and adress")
    }
  }

  render() {
    return (
      <div>
        <div>
          Send a new transaction
        </div>
        <div>
          Token Amount
        </div>
        <input type="number" onChange={this.setAmount.bind(this)} value={this.state.amount}  />
        <div>
          Recipient Adress
        </div>
        <input type="text" onChange={this.setRecipientAddress.bind(this)} value={this.state.address}  />
        <div 
          onClick={this.sendTokens.bind(this)} 
          style={{
            marginTop:"0.5em",
            marginLeft:"40vw",
            borderRadius:"1em",
            padding:"0.25em",
            color:"white",
            width:"7em",
            backgroundColor:"blue"
          }} >
          SEND
        </div>
      </div>
    );
  }
}

export default SendTokens;
