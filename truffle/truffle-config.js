const HDWalletProvider = require('truffle-hdwallet-provider');

const fs = require('fs')

const mnemonic = fs.readFileSync(".secret").toString().trim();

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // Match any network id
      rpc: {
        host: 'localhost',
        port: 8545
      }
    },
    rinkeby: {
      provider: () => new HDWalletProvider(mnemonic, 'https://rinkeby.infura.io/2FBsjXKlWVXGLhKn7PF7'),
      network_id: 4
    },
    kovan: {
      provider: () => new HDWalletProvider(mnemonic, 'https://kovan.infura.io/2FBsjXKlWVXGLhKn7PF7'),
      network_id: 42
    },
    ropsten: {
      provider: new HDWalletProvider(mnemonic, "https://ropsten.infura.io/2FBsjXKlWVXGLhKn7PF7"),
      network_id: 3
    }
  },
  compilers: {
    solc: {
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        }
      }
    }
  }
};
